const Wallet = require('../wallet');

class WalletSetup {
	constructor() {
		/* Constructor for the WalletSetup class */

		this.wallets = [];
	}

	createWallet() {
		/* Create a new wallet instance */

		const newWallet = new Wallet();

		if (newWallet) {
			this.wallets.push(JSON.parse(JSON.stringify(newWallet)));
		}

		return newWallet;
	}

	reformatWallet(walletInstance) {
		/* Reformat wallet instance */

		let newWalletInstance = new Wallet();

		newWalletInstance.balance = walletInstance.balance;
		newWalletInstance.publicKey = walletInstance.publicKey;

		return newWalletInstance;
	}

	hookWallet(walletInstance) {
		/* Hook wallet instance to class */

		let toHookWallet = new Wallet();

		toHookWallet.balance = walletInstance.balance;
		toHookWallet.publicKey = walletInstance.publicKey;

		let isAlreadyHooked = false;

		for (let i = 0; i < this.wallets.length; i++) {
			if (this.wallets[i].publicKey == toHookWallet.publicKey) {
				isAlreadyHooked = true;
			}
		}

		if (!isAlreadyHooked) {
			this.wallets.push(toHookWallet);
		}

		console.log('wallets', this.wallets);
	}

	unhookWallet(toUnhookWallet) {
		/* Unhook wallet from wallet instance */

		const walletInstances = this.wallets.filter(wallet => {
			return wallet != toUnhookWallet
		});

		this.wallets = walletInstances;
	}

	retrieveWallet(toRetrieveWallet) {
		/* Retrieve wallet from class instance */

		for (let i = 0; i < this.wallets.length; i++) {
			if (this.wallets[i] == toRetrieveWallet) {
				return this.wallets[i];
			}
		}

		return false;
	}

	retrieveWalletPrivate(privateKey) {
		/* Retrieve wallet from class instance */

		for (let i = 0; i < this.wallets.length; i++) {
			if (this.wallets[i].publicKey == privateKey) {
				return this.wallets[i];
			}
		}

		return false;
	}
}

module.exports = WalletSetup;