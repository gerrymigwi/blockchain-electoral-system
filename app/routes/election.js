const express = require('express');
const router = express.Router();

const routeConstants = require('./index');

// Get route to view all unverified votes
router.get('/commission', (req, res) => {

	res.json(routeConstants.commissionPool.validVotes());
});

// Get route for finding an election
router.get('/election/:id', (req, res) => {

	let election = null;

	if (req.params.id) {
		election = routeConstants.Election.findElection(req.params.id, routeConstants.blockchain);
	}

	res.json(
		election
	);
});

// Get route for getting election balance
router.get('/election/balance/:id', (req, res) => {

	let election = null;
	let balance = null;

	if (req.params.id) {
		election = routeConstants.Election.findElection(req.params.id, routeConstants.blockchain);
	}

	if (election) {
		balance = routeConstants.Election.calculateBalance(routeConstants.blockchain, election);
	}

	balance ? res.json({
		'status': routeConstants.VERIFIED,
		balance
	}) : res.json({
		'status': routeConstants.UNVERIFIED
	});
});

// Get route for getting current election results
router.get('/election/results/:id', (req, res) => {

	let election = null;
	let electionVotes = null;

	if (req.params.id) {
		election = routeConstants.Election.findElection(req.params.id, routeConstants.blockchain);
	}

	if (election) {
		electionVotes = routeConstants.Election.getResults(routeConstants.blockchain, election);
	}

	electionVotes ? res.json({
		'status': routeConstants.VERIFIED,
		electionVotes
	}) : res.json({
		'status': routeConstants.UNVERIFIED
	});
});

// Post route to create an election
router.post('/create-election', (req, res) => {

	const {
		electorate,
		contenders,
		election_name,
		seed_amount,
		walletInstance,
		start_time,
		end_time
	} = req.body;

	let electionInfo = null;

	const wallet = req.body.walletInstance;
	const retrievedWallet = routeConstants.walletSetup.reformatWallet(wallet);

	routeConstants.walletInstances.addWallet(retrievedWallet);
	const walletInst = routeConstants.walletInstances.retrieveWallet(retrievedWallet);

	if (walletInst) {
		const miner = new Miner(
			routeConstants.blockchain,
			routeConstants.transactionPool,
			routeConstants.commissionPool,
			walletInst,
			routeConstants.p2pServer);

		if (election_name && seed_amount && start_time && end_time && electorate && contenders && miner) {
			electionInfo = walletInst.createElection(election_name, seed_amount, start_time, end_time, electorate,
				contenders, miner);

			electionInfo ? res.json({
				status: routeConstants.VERIFIED,
				electionInfo
			}) : res.json({
				status: routeConstants.UNVERIFIED
			});
		} else {
			res.json({
				status: routeConstants.UNVERIFIED
			});
		}

	} else {
		res.json({
			status: routeConstants.UNVERIFIED
		});
	}
});

// Post route for voter verification
router.post('/voter/verify', (req, res) => {

	const {
		voter_link
	} = req.body;

	let election = null;
	const walletInstance = routeConstants.walletSetup.retrieveWallet();

	if (voter_link) {
		election = walletInstance.verifyVoter(voter_link, routeConstants.blockchain, walletInstance.publicKey);

		election && election.status && election.status === routeConstants.VERIFIED ? res.json({
			'status': routeConstants.VERIFIED,
			voter_link,
			election: election.election
		}) : res.json({
			'status': routeConstants.UNVERIFIED
		});
	} else {
		res.json({
			'status': routeConstants.INCOMPLETE_DATA
		});
	}
});

// Post route for casting a vote
router.post('/voter/cast', (req, res) => {

	const {
		vote,
		voter_link,
		election
	} = req.body;

	let verifiedVote = null;
	const walletInstance = routeConstants.walletSetup.retrieveWallet();

	if (voter_link && election && vote) {
		verifiedVote = walletInstance.castVote(voter_link, routeConstants.blockchain, election, vote,
			routeConstants.commissionPool, walletInstance.keyPair, walletInstance.publicKey);

		if (verifiedVote) routeConstants.p2pServer.broadcastVote(verifiedVote.vote, verifiedVote.signature, vote.keyHash, election.id);

		verifiedVote ? res.json({
			'status': routeConstants.VERIFIED,
			verifiedVote
		}) : res.json({
			'status': routeConstants.UNVERIFIED
		});
	} else {
		res.json({
			'status': routeConstants.INCOMPLETE_DATA
		});
	}
});

module.exports = router;