# Demos - Blockchain Electoral System


## Overview

This repository contains the code required to build the Demos Blockchain Electoral System. 
Demos is a custom Javascript blockchain protocol that enables institutions such as high schools and universities to host and run student elections via the blockchain.
It also makes public the third party libraries that
are currently needed to successfuly build the project. These are as defined in `package.json`.
  
## Build instructions

- Install [Node.js](https://nodejs.org) version 10
- Install all dependancies `npm install`
- Run:
  - To test, run `npm run dev`
  - To run in production, run `npm run start`

## Testing

To test the DEMOS protocol, run `npm run dev`. For further testing, connect the protocol to external dedicated servers such as Heroku.

## Production

To run the Demos protocol in production, run `npm run start` and connect the protocol to external dedicated servers such as Heroku.

## Publications

- African Leadership Academy [How African Blockchain Initiative Aims to Transfer Power to the People](https://www.africanleadershipacademy.org/blog/african-blockchain-initiative/)

## Contributing

Run tests with `npm run dev`.

Pull requests are currently being accepted on the `master` branch.

###### Happy hacking,
Gerry
