const AppUtil = require('../app-util');
const ChainUtil = require('../chain-util');

class CommissionPool {
	constructor() {
		/* Constructor for the CommissionPool class */

		this.votes = [];
	}

	addVote(vote, voteSignature, keyHash, electionId) {
		/* Add a vote to the CommissionPool */

		const newVote = {
			vote: {
				id: vote.id,
				election_id: electionId,
				signature: voteSignature,
				keyHash,
				vote: vote.vote
			}
		}

		this.votes.push(newVote);

		return newVote;
	}

	clear() {
		/* Clear all votes from the CommissionPool */

		this.votes = [];
	}

	validVotes() {
		/* Return all valid votes from the CommissionPool */

		let validVotes = [];

		for (let i = 0; i < this.votes.length; i++) {
			let isDuplicate = false;

			for (let j = 0; j < validVotes.length; j++) {
				if (validVotes[j].vote.keyHash == this.votes[i].vote.keyHash) {
					isDuplicate = true;
				}
			}

			if (!isDuplicate) validVotes.push(this.votes[i]);
		}

		return validVotes;
	}
}

module.exports = CommissionPool;