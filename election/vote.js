const _ = require('lodash');
const Election = require('./index');
const AppUtil = require('../app-util');
const ChainUtil = require('../chain-util');
const Report = require('../app/routes/report');
const {
	ERROR,
	VERIFIED
} = require('../app-constants');

class Vote {
	constructor() {
		/* Constructor for the Vote class */

		this.id = ChainUtil.id();
		this.timeStamp = Date.now();
		this.vote = null;
	}

	static verifyVoter(voterLink, blockchain, publicKey) {
		/* Cast a new vote to the election if it is valid */

		const electionId = voterLink.substring(0, voterLink.indexOf('/'));
		const hashString = voterLink.substring(voterLink.indexOf('/') + 1);

		if (!electionId || !hashString) return Report.report('The voter link provided is invalid.', ERROR);

		const election = Election.findElection(electionId, blockchain);
		if (!election) return Report.report('The URL provided does not match to any election that you are a part of.', ERROR);
		const decryptedVotingData = AppUtil.decryptVotingData(hashString, electionId);
		if (!decryptedVotingData) return Report.report('The voter link provided is invalid.', ERROR);

		let electionDecryptedElectorate = [];
		election.election.electorate.data.forEach((voter, index) => {
			if (index > 0) {
				const voterEmail = AppUtil.decryptEmailAddress(voter[2], election.election.publicKey);
				electionDecryptedElectorate.push([
					voter[0], voter[1], voterEmail
				]);
			}
		});

		let isEligibleToVote = false;

		electionDecryptedElectorate.forEach(decryptedVoter => {
			if (_.isEqual(decryptedVotingData, decryptedVoter)) {
				isEligibleToVote = true;
			}
		});

		const hasVotedBefore = this.hasVotedBefore(blockchain, election.election.id, publicKey);

		if (!isEligibleToVote) return Report.report('Sorry. You are not eligible to participate in this election.', ERROR);
		if (hasVotedBefore) return Report.report('Sorry. You can only vote once.', ERROR);

		const result = {
			status: VERIFIED,
			election
		}

		return result;
	}

	static hasVotedBefore(blockchain, electionId, publicKey) {
		/* Check whether a voter has already voted */

		let voteBlocks = [];
		let hasVotedBefore = false;

		for (let i = 0; i < blockchain.chain.length; i++) {
			if (blockchain.chain[i].data.vote) {
				voteBlocks.push(blockchain.chain[i].data);
			}
		}

		for (let i = 0; i < voteBlocks.length; i++) {
			if (voteBlocks[i].vote.keyHash == ChainUtil.hash(`${publicKey}${electionId}${voteBlocks[i].vote.vote}`).toString()) {
				hasVotedBefore = true;
			}
		}

		return hasVotedBefore;
	}

	sign(dataHash, walletKeyPair) {
		/* Handle the signing of new votes */

		return walletKeyPair.sign(dataHash);
	}

	castVote(vote, commissionPool, walletKeyPair, walletPublicKey, electionId) {
		/* Cast a voter's vote in the election */

		const voteSignature = this.sign(ChainUtil.hash(JSON.stringify(vote.vote)), walletKeyPair);
		const keyHash = ChainUtil.hash(`${walletPublicKey}${electionId}${vote.vote}`).toString();

		const verifiedVote = commissionPool.addVote(vote, voteSignature, keyHash, electionId);

		return verifiedVote;
	}
}

module.exports = Vote;