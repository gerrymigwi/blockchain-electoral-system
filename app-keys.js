// Debug API_SERVICE_URL : http://localhost:8080/api/0.1.0
// Production API_SERVICE_URL : https://aeon-api-service.herokuapp.com/api/0.1.0

const APP_EMAIL_PASSWORD = '<APP_EMAIL_PASSWORD>';
const APP_EMAIL = '<APP_EMAIL>';
const API_SERVICE_URL = 'https://aeon-api-service.herokuapp.com/api/0.1.0';

module.exports = {
	APP_EMAIL,
	API_SERVICE_URL,
	APP_EMAIL_PASSWORD
};